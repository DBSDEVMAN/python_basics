# Welcome to Python Basics! #

### What is Python Basics? ###

This is a 7 week course on Python. The intent is to establish a basic understanding of the Python language and to be able to perform basic data analysis by the end of the course. This course will not teach you everything there is to know about Python. In an effort to expedite the learning process and make the most out of the time put into this course, some Python concepts may not be covered or go into much detail. Hopefully, this course will serve as a taste of things to come and will spark you to dive deeper into the sea of Python.

### What's Needed? ###

You will need access to Pluralsight and a bitbucket account.
> All setup and work should be completed on a Daugherty machine.

### Course Expectations ###

* Each week will consist of:
    * An hour long class on Wednesday evening
    * 4 - 6 hours of homework including: Pluralsight courses and a coding assessment
* Homework should be submitted by 3pm EST Tuesday afternoons. 
* Week 7 will consist of a final project utilizing all of the skills learned in the course
* In order to receive credit for the course:
    * At least 4 out of the 5 classes must be attended
    * All homework assessments must be submitted
    * Final project must be completed 

### Course Structure ###

* Week 1 - Installtion and Setup; Intro to Jupyter Notebooks
* Week 2 - Data types, Operators, and Collections
* Week 3 - Loops and Functions
* Week 4 - Playing with Pandas
* Week 5 - Visualizations
* Week 6 - APIs
* Week 7 - Project

### Primary Course Resources ###
* Week 2 - [Python for Data Analysts](https://app.pluralsight.com/library/courses/python-data-analysts/table-of-contents) First 3 sections
* Week 3 - [Python for Data Analysts](https://app.pluralsight.com/library/courses/python-data-analysts/table-of-contents) Remaining sections (Last section can be considered optional but recommended)
* Week 4 - [Pandas Playbook: Manipulating Data](https://app.pluralsight.com/library/courses/pandas-playbook-manipulating-data/table-of-contents)
* Week 5 - [Introduction to Data Visualization with Python](https://app.pluralsight.com/library/courses/data-visualization-with-python-introduction/table-of-contents)
* Week 6 - [Python & APIs: A Winning Combo for Reading Public Data](https://realpython.com/python-api/)

### Optional Resources ###
> **Note:** Don't use these cheat sheets as a replacement for documentation. Rather, use these to help guide your documentation search

* [Python for Beginners Playlist by Microsoft](https://www.youtube.com/playlist?list=PLlrxD0HtieHhS8VzuMCfQD4uJ9yne1mE6) - Several video that may supplement your knowledge throughout the course and project
* [Python Cheat Sheet](https://hakin9.org/python-cheat-sheet-for-hackers-and-developers/)
* [Pandas Cheat Sheet](https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf)
* [Visualization Cheat Sheets](https://python-graph-gallery.com/cheat-sheets/)
* [Instructor Answer Keys](https://www.youtube.com/watch?v=dQw4w9WgXcQ)


### Submitting Homework ###

TBD

### Who do I talk to if I have a question? ###

* Payson Chadrow - payson.chadrow@daugherty.com
* Amanda Matheu - amanda.matheu@daugherty.com